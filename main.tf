
##module "ssh-key" {

#   source = "./modules/ssh-key"
#   key = var.key
# }

module "vpc" {

  source   = "./modules/vpc"
  vpc_cidr = var.vpc_cidr
  vpc_name = var.vpc_name

}

module "subnet" {

  source = "./modules/subnet"
  vpc_id = module.vpc.vpc_id
  # vpc_cidr_block = module.vpc.vpc_cidr_block
  subnet_cidr = var.subnet_cidr
  subnet_name = var.subnet_name
  depends_on = [
    module.vpc
  ]
}

module "securitygroup" {

  source  = "./modules/security-group"
  vpc_id  = module.vpc.vpc_id
  sg_name = var.sg_name
  ports   = var.ports
  depends_on = [
    module.subnet
  ]


}

module "instance" {

  source        = "./modules/instance"
  instance_Name = var.instance_Name
  instance_ami  = var.instance_ami
  instance_type = var.instance_type
  depends_on = [
    module.securitygroup
  ]
  security_groups = module.securitygroup.sg_id
  subnet_id       = module.subnet.subnet_id

}
