
vpc_cidr = "10.0.0.0/16"

vpc_name = {
  "Name" = "tf-example"
}

subnet_cidr = "10.0.1.0/24"

subnet_name = {
  "Name" = "my_subnet"
}

sg_name = {
  "Name" = "allow_tls"
}

ports = [22, 3306, 443, 80, 8080, 6443, 0]

instance_Name = {
  "Name" = "Prod-Server"
}

instance_ami = "ami-0c4f7023847b90238"

instance_type = "t2.medium"
