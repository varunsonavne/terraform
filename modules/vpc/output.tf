output "vpc_id" {

    value = aws_vpc.my_vpc.id
  
}

output "vpc_cidr_block" {
    value = aws_vpc.my_vpc.cidr_block
}

