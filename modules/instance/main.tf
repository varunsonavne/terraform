resource "aws_instance" "foo" {

    ami = var.instance_ami
    instance_type = var.instance_type
    tags = "${var.instance_Name}"
    key_name = "wwww"
    vpc_security_group_ids = ["${var.security_groups}"]
    subnet_id = var.subnet_id
    associate_public_ip_address=true
    user_data              = file("${path.module}/script.sh")
    

    # vpc_security_group_ids = ["${var.security_groups}"]
    #security_groups = var.security_groups
    #  vpc_security_group_ids = ["${aws_security_group.allow_tls.id}"]

}



