
variable "vpc_id" {
    type = string
}

# variable "vpc_cidr_block" {
#     type = string
# }

variable "subnet_cidr" {

    type = string
    default = "172.31.16.0/20"
}

variable "subnet_name"{
    type = map(any)
    default = {
        "Name" = "my_subnet"
    }
}


